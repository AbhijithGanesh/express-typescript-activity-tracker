import type { Express, Request, Response } from "express";
import express, { json } from "express";

const PORT: number = 8000; //environment variable
let app: Express = express();

app.use(json()); // Express should allow json. Parse JSON

app.get("/", (req: Request, res: Response): void => {
  if (req.body?.name == "Abhijith") {
    res.status(200).send({
      Message: " Hello Abhijith",
    });
  } else {
    res.status(200).send({
      Message: "Hello World",
    });
    console.log(req.body?.name);
  }
});

app.listen(PORT, () => {
  `The app is listening on port ${PORT}`;
});
