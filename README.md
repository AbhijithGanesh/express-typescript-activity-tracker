# Express-Typescript-Activity-Tracker

The repository for the activity tracker app made using postgres, prisma and typescript. Checkout the youtube tutorial: https://www.youtube.com/channel/UCT8CkbAxnsiuFtHSRsll25Q

The project directory will be filled with sub-directories with each sub-directory denoting a tutorial. This is done keeping to accomodate the needs of the new learners who don't understand git.

We'll be using TypeScript, Postgres, Prisma and React (Gatsby) for the whole application.
